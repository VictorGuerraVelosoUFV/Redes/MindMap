<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Redes de computadores" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1633861995" CREATED="1521684251628" MODIFIED="1521930279623"><hook NAME="MapStyle" zoom="0.932">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Divis&#xe3;o em escala" FOLDED="true" POSITION="right" ID="ID_1408929415" CREATED="1521684277232" MODIFIED="1521684280152">
<node TEXT="WAN" FOLDED="true" ID="ID_1369087030" CREATED="1521684431903" MODIFIED="1521684433894">
<node TEXT="Rede de Longo Alcance" ID="ID_939418289" CREATED="1521684568968" MODIFIED="1521684589991"/>
</node>
<node TEXT="MAN" FOLDED="true" ID="ID_710752068" CREATED="1521684434263" MODIFIED="1521684437726">
<node TEXT="Rede metropolitana" ID="ID_1960879493" CREATED="1521684559918" MODIFIED="1521684566705"/>
</node>
<node TEXT="LAN" FOLDED="true" ID="ID_1607962381" CREATED="1521684438642" MODIFIED="1521684439793">
<node TEXT="Rede local" ID="ID_1814977467" CREATED="1521684553067" MODIFIED="1521684557981"/>
</node>
<node TEXT="PAN" FOLDED="true" ID="ID_1555043230" CREATED="1521684440560" MODIFIED="1521684442061">
<node TEXT="Embarcados" ID="ID_1320410583" CREATED="1521684503366" MODIFIED="1521684550433"/>
<node TEXT="Sensores" ID="ID_426230353" CREATED="1521684591578" MODIFIED="1521684595174"/>
</node>
<node TEXT="BAN" FOLDED="true" ID="ID_709779111" CREATED="1521684446708" MODIFIED="1521684448228">
<node TEXT="Implantes no corpo" ID="ID_97367614" CREATED="1521684493814" MODIFIED="1521684501278"/>
</node>
</node>
<node TEXT="Divis&#xe3;o por tecnologia de transmiss&#xe3;o" FOLDED="true" POSITION="right" ID="ID_1913565645" CREATED="1521684280742" MODIFIED="1521684417656">
<node TEXT="Difus&#xe3;o" ID="ID_805027866" CREATED="1521684419693" MODIFIED="1521684425420"/>
<node TEXT="Ponto-a-ponto" ID="ID_570302179" CREATED="1521684425849" MODIFIED="1521684429788"/>
</node>
<node TEXT="Divis&#xe3;o em camadas" FOLDED="true" POSITION="left" ID="ID_46613847" CREATED="1521684267635" MODIFIED="1521684276319">
<node TEXT="Camada F&#xed;sica" ID="ID_1119203855" CREATED="1521684600608" MODIFIED="1521684607265"/>
<node TEXT="Camada de Rede" ID="ID_1042360648" CREATED="1521684607697" MODIFIED="1521684612598"/>
<node TEXT="Camada de Transporte" ID="ID_1742061560" CREATED="1521684613218" MODIFIED="1521684617655"/>
<node TEXT="Camada de Enlace" ID="ID_1198255525" CREATED="1521684617996" MODIFIED="1521684625529"/>
<node TEXT="Subcamada de controle de acesso" ID="ID_1415716911" CREATED="1521684626124" MODIFIED="1521684647939"/>
</node>
</node>
</map>
